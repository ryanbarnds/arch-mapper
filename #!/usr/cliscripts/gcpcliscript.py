# pip3 install oauth2client google-api-python-client
# python3 gcpcliscript.py --projects [project_id [project_ids...]] [-o <output file>]
from googleapiclient import discovery
from oauth2client.client import GoogleCredentials
from datetime import datetime, tzinfo, timedelta
from argparse import ArgumentParser
import os
import platform

import hashlib
import json

ERROR_COLOR = '\033[91m'
WARNING_COLOR = '\033[93m'
END_COLOR = '\033[0m'

# Setting up the timezone to be UTC
class SimpleUtc(tzinfo):
    def tzname(self):
        return "UTC"

    def utcoffset(self, dt):
        return timedelta(0)

# Creating a new json encoder that uses the UTC timezone
class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.utcnow().replace(tzinfo=SimpleUtc()).isoformat()

        return json.JSONEncoder.default(self, o)

def print_err(msg, warning=False):
    start_color = WARNING_COLOR if warning else ERROR_COLOR
    print(start_color + msg + END_COLOR)


def encrypt_string(raw_string):
    return hashlib.sha256(raw_string.encode()).hexdigest()

def print_to_file(contents, file_name, message, pretty_print=False):
    with open(file_name, 'w') as f:
        indent = 4 if pretty_print else None
        json.dump(contents, f, indent=indent, cls=DateTimeEncoder)
        print(message)

def get_instance_group_instances(project, compute_service):
    result = []
    instance_groups = get_resource('instanceGroups', project, compute_service.instanceGroups().aggregatedList, compute_service.instanceGroups().aggregatedList_next)
    for instance_group in instance_groups:
        try:
            if 'zone' in instance_group:
                # zone-based instance group
                instance_group_zone = instance_group['zone'].split('/')[-1]
                request = compute_service.instanceGroups().listInstances(project=project, zone=instance_group_zone, instanceGroup=instance_group['name'])
                instance_group_instances = []
                while request is not None:
                    response = request.execute()
                    instance_group_instances.extend(response['items'])
                    request = compute_service.instanceGroups().listInstances_next(previous_request=request, previous_response=response)
                instance_group['instances'] = instance_group_instances
            else:
                # region-based instance group
                instance_group_region = instance_group['region'].split('/')[-1]
                request = compute_service.regionInstanceGroups().listInstances(project=project, region=instance_group_region, instanceGroup=instance_group['name'])
                instance_group_instances = []
                while request is not None:
                    response = request.execute()
                    instance_group_instances.extend(response['items'])
                    request = compute_service.regionInstanceGroups().listInstances_next(previous_request=request, previous_response=response)
                instance_group['instances'] = instance_group_instances

            result.append(instance_group)

        except Exception as e:
            print_err(str(e))

    return result

def get_bigquery_dataset_metadata(project, bigquery_service):
    result = []
    partial_dataset_metadata = get_project_resource('bigQueryDatasets', project, bigquery_service.datasets().list, bigquery_service.datasets().list_next, requestKey='projectId', responseField='datasets')
    for dataset in partial_dataset_metadata:
        try:
            request = bigquery_service.datasets().get(projectId=project, datasetId=dataset['datasetReference']['datasetId'])
            response = request.execute()
            result.append(response)

        except Exception as e:
            print_err(str(e))

    return result

def get_bigtable_resources(project, bigtable_service):
    instance_api = bigtable_service.projects().instances()
    instances = get_project_resource('bigtable instances', f'projects/{project}', instance_api.list, instance_api.list_next, requestKey='parent', responseField='instances')
    for instance in instances:
        instance['clusters'] = get_project_resource('bigtable instance clusters', instance['name'], instance_api.clusters().list, instance_api.clusters().list_next, requestKey='parent', responseField='clusters')
        for cluster in instance['clusters']:
            cluster['backups'] = get_project_resource('bigtable instance cluster backups', cluster['name'], instance_api.clusters().backups().list, instance_api.clusters().backups().list_next, requestKey='parent', responseField='backups')
        instance['tables'] = get_project_resource('bigtable instance tables', instance['name'], instance_api.tables().list, instance_api.tables().list_next, requestKey='parent', responseField='tables')
        instance['appProfiles'] = get_project_resource('bigtable instance appProfiles', instance['name'], instance_api.appProfiles().list, instance_api.appProfiles().list_next, requestKey='parent', responseField='appProfiles')

    return instances

def get_project_resource(resource, project, list_function, list_next_function, requestKey='project', responseField='items', region=None):
    result = []
    if resource is not None:
        print('Executing ' + resource)
    try:
        if region != None:
            request = list_function(**{requestKey: project, 'region': region})
        else:
            request = list_function(**{requestKey: project})
        while request is not None:
            response = request.execute()

            if response.get(responseField):
                result.extend(response[responseField])

            if response.get('unreachable'):
                for location in response['unreachable']:
                    print(f'	Unreachable location: {location}')

            if response.get('failedLocations'):
                for location in response['failedLocations']:
                    print(f'	Failed location: {location}')

            request = list_next_function(previous_request=request, previous_response=response)

    except Exception as e:
        result = []
        print_err(str(e))

    return result

def get_resource(resource, project, list_function, list_next_function):
    result = []
    print('Executing ' + resource)
    try:
        request = list_function(project=project)
        while request is not None:
            response = request.execute()

            locations = response.get('items', {})
            for location in locations.values():
                result.extend(location.get(resource, []))

            request = list_next_function(
                previous_request=request, previous_response=response)

    except Exception as e:
        result = []
        print_err(str(e))

    return result

def get_regions(project, service):
	regions = get_project_resource('regions', project, service.regions().list, service.regions().list_next)
	names = []
	for region in regions:
		if 'name' in region:
			names.append(region['name'])
	return names

def get_project_resource_with_regions(resource, project, list_function, list_next_function, regions, requestKey='project', responseField='items'):
    result = []
    print('Executing ' + resource)
    for region in regions:
        response = get_project_resource(None, project, list_function, list_next_function, requestKey=requestKey, responseField=responseField, region=region)
        result.extend(response)
    return result

def main():
    # this enables colorization in windows cmd terminal
    if "Windows" in platform.system():
        os.system('color')
    parser = ArgumentParser()
    parser.add_argument('-p', '--projects', type=str, nargs='+')
    parser.add_argument(
        '-o', '--output', help="specify output file name", nargs='?', type=str, action='store')
    args = parser.parse_args()
    OUT_FILE = args.output
    if OUT_FILE and not OUT_FILE.endswith(".json"):
        OUT_FILE += ".json"
    projects = args.projects

    if not projects:
        print_err("Usage: gcpcliscript.py --projects [project_id [project_ids...]] [-o <output file>]")
        exit()

    output = {'projects': []}

    credentials = GoogleCredentials.get_application_default()

    cloud_resource_manager = discovery.build('cloudresourcemanager', 'v1', credentials=credentials)

    bigtable_service = discovery.build('bigtableadmin', 'v2', credentials=credentials)
    bigquery_service = discovery.build('bigquery', 'v2', credentials=credentials)
    cloud_function_service = discovery.build('cloudfunctions', 'v1', credentials=credentials)
    compute_service = discovery.build('compute', 'v1', credentials=credentials)
    container_service = discovery.build('container', 'v1', credentials=credentials)
    dns_service = discovery.build('dns', 'v1', credentials=credentials)
    sql_service = discovery.build('sqladmin', 'v1beta4', credentials=credentials)
    storage_service = discovery.build('storage', 'v1', credentials=credentials)

    for project in projects:
        project_request = cloud_resource_manager.projects().get(projectId=project)
        project_obj = project_request.execute()
        projectNumber = project_obj.get('projectNumber')
        resources = {}

        print('Getting resources for project: ' + project)

        regions = get_regions(project, compute_service)

        resources['compute'] = {}
        resources['compute']['instances'] = get_resource('instances', project, compute_service.instances().aggregatedList, compute_service.instances().aggregatedList_next)
        resources['compute']['autoscalers'] = get_resource('autoscalers', project, compute_service.autoscalers().aggregatedList, compute_service.autoscalers().aggregatedList_next)
        resources['compute']['cloudFunctions'] = get_project_resource('cloudFunctions', f'projects/{project}/locations/-', cloud_function_service.projects().locations().functions().list, cloud_function_service.projects().locations().functions().list_next, requestKey='parent', responseField='functions')
        resources['compute']['gkeClusters'] = get_project_resource('gkeClusters', f'projects/{project}/locations/-', container_service.projects().locations().clusters().list, lambda previous_request,previous_response: None, requestKey='parent', responseField='clusters')
        resources['compute']['instanceGroups'] = get_instance_group_instances(project, compute_service)

        resources['network'] = {}
        resources['network']['vpcNetworks'] = get_project_resource('networks', project, compute_service.networks().list, compute_service.networks().list_next)
        resources['network']['subnetworks'] = get_resource('subnetworks', project, compute_service.subnetworks().aggregatedList, compute_service.subnetworks().aggregatedList_next)
        resources['network']['firewallRules'] = get_project_resource('firewalls', project, compute_service.firewalls().list, compute_service.firewalls().list_next)
        resources['network']['vpnGateways'] = get_resource('vpnGateways', project, compute_service.vpnGateways().aggregatedList, compute_service.vpnGateways().aggregatedList_next)
        resources['network']['routers'] = get_resource('routers', project, compute_service.routers().aggregatedList, compute_service.routers().aggregatedList_next)
        resources['network']['routes'] = get_project_resource('routes', project, compute_service.routes().list, compute_service.routes().list_next)
        resources['network']['forwardingRules'] = get_resource('forwardingRules', project, compute_service.forwardingRules().aggregatedList, compute_service.forwardingRules().aggregatedList_next)
        resources['network']['targetHttpProxies'] = get_project_resource('targetHttpProxies', project, compute_service.targetHttpProxies().list, compute_service.targetHttpProxies().list_next)
        resources['network']['targetHttpsProxies'] = get_project_resource('targetHttpsProxies', project, compute_service.targetHttpsProxies().list, compute_service.targetHttpsProxies().list_next)
        resources['network']['backendServices'] = get_resource('backendServices', project, compute_service.backendServices().aggregatedList, compute_service.backendServices().aggregatedList_next)
        resources['network']['backendBuckets'] = get_project_resource('backendBuckets', project, compute_service.backendBuckets().list, compute_service.backendBuckets().list_next)
        resources['network']['urlMaps'] = get_resource('urlMaps', project, compute_service.urlMaps().aggregatedList, compute_service.urlMaps().aggregatedList_next)
        resources['network']['regionUrlMaps'] = get_project_resource_with_regions('regionUrlMaps', project, compute_service.regionUrlMaps().list, compute_service.regionUrlMaps().list_next, regions)

        resources['network']['managedZones'] = get_project_resource('managedZones', project, dns_service.managedZones().list, dns_service.managedZones().list_next, responseField='managedZones')

        resources['storage'] = {}
        resources['storage']['buckets'] = get_project_resource('buckets', project, storage_service.buckets().list, storage_service.buckets().list_next)
        resources['storage']['persistentDisks'] = get_resource('disks', project, compute_service.disks().aggregatedList, compute_service.disks().aggregatedList_next)

        resources['database'] = {}
        resources['database']['cloudSqlInstances'] = get_project_resource('sql instances', project, sql_service.instances().list, sql_service.instances().list_next)
        resources['database']['cloudBigtableInstances'] = get_bigtable_resources(project, bigtable_service)

        resources['analytics'] = {}
        resources['analytics']['bigQueryDatasets'] = get_bigquery_dataset_metadata(project, bigquery_service)

        output['projects'].append({'projectId': project, 'projectNumber': projectNumber, 'resources': resources})

    out_file = OUT_FILE if OUT_FILE else 'gcp.json'
    print_to_file(output, out_file, '\nOutput to ' + out_file)

if __name__ == "__main__":
    main()
